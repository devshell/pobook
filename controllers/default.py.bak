# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - api is an example of Hypermedia API support and access control
#########################################################################
'''
http://127.0.0.1:8000/omipo/default/user/register
http://127.0.0.1:8000/omipo/default/user/login?_next=/omipo/default/index
http://127.0.0.1:8000/omipo/default/user/profile
http://127.0.0.1:8000/omipo/default/user/change_password
http://127.0.0.1:8000/omipo/default/user/logout?_next=/omipo/default/index

'''
#TODO add ability to have multiple team members in the role of PM or Super and their view changes based on that -- Additional Groups & Memberships?
@auth.requires_login()
def index():

    admin = db(db.auth_membership.user_id == session.auth.user.id).select(db.auth_membership.group_id)
    if len(admin) == 0:
        admin = None
        
    projects = db(
        (db.project.id == db.team.project_id)&
        (db.auth_user.id == db.team.member_id) &
        (db.project.closed == False)
    )

    all_projects = projects(db.team.member_id == session.auth.user.id).select(db.project.id, db.project.num, db.project.name).as_list()
    if len(all_projects) == 0:
        all_projects = None
        
    pm_projects = projects((db.team.role == "pm") & (db.team.member_id == session.auth.user.id)).select(db.team.project_id).as_list()
    if len(pm_projects) == 0:
        pm_projects = None

    sup_projects = projects((db.team.role == "super") & (db.team.member_id == session.auth.user.id)).select(db.team.project_id).as_list()
    if len(sup_projects) == 0:
        sup_projects = None
        
    last_project = db(db.settings.user_id == session.auth.user.id).select()
    if len(last_project) == 0:
        last_project = None
    else:
        if not last_project in all_projects: last_project = None

    session.pm_projects = None
    session.sup_projects = None
    session.projects = None
    session.project = None
    session.budget_total = 0
    session.budget_spent = 0

    if pm_projects != None:
        session.pm_projects = []
        for p in pm_projects:
            session.pm_projects.append(p['project_id'])
    if sup_projects != None:
        session.sup_projects = []
        for s in sup_projects:
            session.sup_projects.append(s['project_id'])
    
    if last_project != None:
        session.project = last_project[0].last_project
    else:
        if pm_projects != None:
            if len(pm_projects) > 0:
                session.project = pm_projects[0]['project_id']
            elif sup_projects != None:
                if len(sup_projects) > 0:
                    session.project = sup_projects[0]['project_id']
        elif sup_projects != None:
            if len(sup_projects) > 0:
                session.project = sup_projects[0]['project_id']
        
    if admin != None:
        if(admin[0].group_id == 1):
            session.admin = True
        else:
            session.admin = False
    else:
        session.admin = False

    if all_projects != None:
        session.projects = all_projects


    session.project_ids = []
    
    if session.projects != None:
        for p in session.projects:
            session.project_ids.append(p['id'])
        
    if session.project == None:
        session.project = 0
        session.budget_total = 0
        session.budget_spent = 0
        redirect(URL('po', 'list', args=['project', session.project]))
        
    elif session.project > 0:
        session.budget_total = getbudget(session.project)
        session.budget_spent = getpototal(session.project)
        redirect(URL('po', 'list', args=['project', session.project]))
        
    else:
        redirect(URL('po', 'list', args=['project', session.project]))
        
    response._vars = "ERROR: No projects in list for sup or pm"
    return dict(
        error={
            'admin': admin, 
            'last_project': last_project, 
            'projects': projects, 
            'all_projects': all_projects, 
            'pm_projects': pm_projects, 
            'sup_projects': sup_projects, 
            'session.pm_projects': session.pm_projects,  
            'session.sup_projects': session.sup_projects, 
            'session.projects': session.projects})

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    
    form=auth()
    if isinstance(form,FORM):
        form[0][-1][1].append(INPUT(_type="button",_value="Change Password",_onclick="window.location='%s';"%URL(c='default',f='user', args='change_password')))
    if request.args(0)=='login':
        form.element(_type='submit')['_value']='Sign In'
    #if request.args(0)=='profile':
    #    response.view='default/user/profile'
    return dict(form=form)
    #return dict(form=auth())


#@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login()
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET':{},'POST':{},'PUT':{},'DELETE':{}},
        }
    return Collection(db).process(request,response,rules)
