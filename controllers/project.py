# -*- coding: utf-8 -*-
# try something like
#@cache.action(time_expire=300, cache_model=cache.ram, quick='SVP')
@auth.requires_login()
def list():
    source = request.vars.source
    here = URL(args=request.args, vars=request.vars)
    
    fields = [db.project.id, db.project.num, db.project.name, db.project.closed]
    headers = {
        'project.id': 'ID', 
        'project.num': 'Project No.',
        'project.name': 'Project Name',
        'project.closed': 'Project Closed',
        'item.budget': 'Budget',
        'item.committed':'Committed',
        'item.budgetbal': 'Budget Balance',
        'item.invoiced': 'Invoiced',
        'item.combal': 'Committment Balance'
    }
    links_all = [
            dict(header='Budget', body=lambda row: DIV(('${:,.2f}'.format(getbudget(row.id))), _style='text-align: right;')),
            dict(header='Committed', body=lambda row: DIV(('${:,.2f}'.format(getpototal(row.id))), _style='text-align: right;')),
            dict(header='Budget Balance', body=lambda row: DIV(('${:,.2f}'.format(getbudgetbal(row.id))), _style='text-align: right;')),
            dict(header='Invoiced', body=lambda row: DIV(('${:,.2f}'.format(getprojectinvoicetotal(row.id))), _style='text-align: right;')),
            dict(header='Committment Balance', body=lambda row: DIV(('${:,.2f}'.format(getprojectinvoicebal(row.id))), _style='text-align: right;')),
            dict(header='Project PO List', body=lambda row: A('Commitments',_class='button btn btn-default',_href=URL('po', 'list', args=['project', row.id]))),
            dict(header='Files', body=lambda row: A('Files',_class='button btn btn-default',_href=URL('attachments', 'list', args=['project',row.id], vars=dict(source=here))))
        ]
    
    maxlengths = {
        'project.id' : 100,
        'project.num' : 200,
        'project.name' : 300,
        'project.closed' : 10,
        'item.budget': 10,
        'item.committed': 10,
        'item.budgetbal': 10,
        'item.invoiced': 10,
        'item.combal': 10
    }
    
    grid = SQLFORM.grid(db.project, orderby=db.project.closed, fields = fields, headers=headers, create=False, deletable=False, editable=False, maxtextlengths=maxlengths, details=False, links=links_all)
    return dict(list=grid)

#TODO: add screen for modifying project properties for PM
@auth.requires_login()
def edit():
    fields = [db.project.num, db.project.name]
    grid = SQLFORM.grid(db.project, fields = fields, create=False, deletable=False, editable=False)
    return dict(list=grid)

@auth.requires_login()
def set():
    if not request.post_vars['project'] == None:
        last_project = int(request.post_vars['project'])
        if last_project in session.project_ids:
            session.project = last_project
            settings = db(db.settings.user_id == session.auth.user.id).select()
            if len(settings):
                settings[0].update_record(last_project = last_project)
            else:
                db.settings.insert(
                            user_id = session.auth.user.id,
                            last_project = last_project)
            db.commit()
        
        if session.project > 0:
            session.budget_total = getbudget(session.project)
            session.budget_spent = getpototal(session.project)
            
    return dict()
