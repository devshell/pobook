# -*- coding: utf-8 -*-

ENV = 'DEV'

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################
# https://web2py.wordpress.com/2010/05/23/some-tricks-to-use-when-dealing-with-databases/
## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    ## db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
    db = DAL('sqlite://storage.sqlite', migrate=False)
    ##db = DAL('mysql://watr:0m1g0fooltheworld@watr.mysql.pythonanywhere-services.com/watr$omigo', fake_migrate_all = True) 
#else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    #db = DAL('google:datastore+ndb')
    ## store sessions and tickets there
    #session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []

## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import *

auth = Auth(db)
service = Service()
plugins = PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.settings.actions_disabled.append('register')
#db.auth_user.password.readable = db.auth_user.password.writable = True
## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
#from gluon.contrib.login_methods.janrain_account import use_janrain
#use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

# main_with_json.py


'''
import json

with open("/home/watr/config.json") as json_data_file:
    config = json.load(json_data_file)

aws_akey = config['DEFAULT'][ENV]['ACCESS_KEY'] # 'public-key-of-myapp'
aws_skey = config['DEFAULT'][ENV]['SECRET_KEY'] # 'secret-key-of-myapp'
region = config['DEFAULT'][ENV]['REGION'] # 'secret-key-of-myapp'
bucket = config['DEFAULT'][ENV]['BUCKET'] # 'secret-key-of-myapp'

'''
aws_akey = "AKIAIRBNHWY46T6TIQVQ"
aws_skey ="pGBowLtThoUtOK7UKZIIhBiOGXkRU1QOwpaPyctW"
region = "ca-central-1"
#bucket = "pobook-dev"
#bucket = "pobook-testing"
bucket = "ivanhoe"

ERRORS = []


from fs_s3 import S3FS
import datetime
import boto3


myfs = S3FS(
            bucket,
            aws_access_key_id=aws_akey,
            aws_secret_access_key=aws_skey,
            region=region
            )


s3conn = boto3.Session(
    aws_access_key_id=aws_akey,
    aws_secret_access_key=aws_skey,
    region_name=region
)

s3 = s3conn.resource("s3")


db.define_table('settings',
                Field('user_id', db.auth_user),
                Field('last_project', 'integer')
               )

db.define_table('project',
                Field('closed','boolean', default=False),
                Field('name','string'),
                Field('num','string'),
                Field('po_prefix','string'),
                Field('last_po', 'integer', default=0),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
               )

## This is for the new feature of having multiple team members on a project and changing roles
db.define_table('team',
                Field('project_id', db.project, requires=IS_IN_DB(db,'project.id', '%(num)s - %(name)s')),
                Field('member_id', db.auth_user, requires=IS_IN_DB(db,'auth_user.id', '%(first_name)s - %(last_name)s')),
                Field('role', 'string', default='pm', requires=IS_IN_SET(['pm','super'])),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )

db.define_table('vendors',
                Field('num', 'string', default=None, requires=IS_NOT_EMPTY()),
                Field('name', 'string', default=None, requires=IS_NOT_EMPTY()),
                Field('address1', 'string', default=None),
                Field('address2', 'string', default=None),
                Field('address3', 'string', default=None),
                Field('city', 'string', default=None),
                Field('region', 'string', default=None),
                Field('country', 'string', default=None),
                Field('post_code', 'string', default=None),
                Field('contractor_check_id', 'integer', default=None),
                Field('contractor_check_expiry', 'date', default=None),
                Field('link', 'string', default=None),
                Field('comments', 'text', default=None),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
               )

#only accessible by PM
db.define_table('budget',
                Field('project_id', db.project), #hidden field - auto assigned by controller function
                Field('added','date'),
                Field('amount', 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field('comments', 'text'),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )


db.define_table('po',
                Field('project_id', db.project),
                Field('vendor_id', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s')),
                Field('num', 'string'),
                Field("holdback", "integer", default=0),
                Field("form_c", "date", default=None),
                Field("expired", "date", default=None),
                Field('closed','date', default=None),
                Field('scope', 'text'), #only accessible by PM
                Field('comments', 'text'), #only accessible by PM
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )

db.define_table('item',
                Field('po_id', db.po), #hidden field - auto assigned by controller function
                Field('description','text'),
                Field('amount', 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )

db.define_table("invoices",
                Field('vendor_id', db.vendors, requires=IS_IN_DB(db,'vendors.id', '%(name)s')),
                Field("num", "string", default=None),
                Field("dated", "date", default=None),
                Field("date_received", "date", default=None),
                Field("date_entered", "date", default=None),
                Field("amount_beforeHoldback", 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field("amount_pretax", 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field("amount_total", 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field("paid_date", "date", default=None),
                Field("paid_doc", "string", default=None),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )


db.define_table("invoice_items",
                Field("invoice_id", db.invoices, requires=IS_IN_DB(db,'invoices.id', '%(num)s')),
                Field('commitment_id', db.po, requires=IS_IN_DB(db,'po.id', '%(num)s')),
                Field("commitment_item_id", db.item, requires=IS_IN_DB(db, 'item.id', '%(id)s')),
                Field("description", "text", default=None),
                Field("amount_beforeHoldback", 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field("amount_pretax", 'double', notnull=True, default=0.00, represent=lambda value, row: DIV(('${:,.2f}'.format(0.0) if value == None else '${:,.2f}'.format(value)), _style='text-align: right;')),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )


# TODO: add attachments list to PO ADD, and EDIT
db.define_table('attachments',
                Field('user_id', db.auth_user),
                Field('project_id', db.project),
                Field('vendor_id', db.vendors),
                Field('commitment_id', db.po),
                Field('budget_id', db.budget),
                #Field('item_id', db.item),
                Field('invoice_id', db.invoices),
                #Field('tax_id', db.taxes),
                Field('name', 'string', requires=IS_NOT_EMPTY()),
                Field('files', 'upload', uploadfs = myfs, requires=IS_NOT_EMPTY()),
                #Field('files', 'upload', autodelete=True, requires=IS_NOT_EMPTY()),
                Field('tag', 'string', default=None),
                Field('updated_on', 'datetime', default=request.now),
                Field('updated_by', db.auth_user, default=auth.user_id), #hidden field
                Field('locked_user', 'integer', default=None)
                )

if db(db.auth_user.id == 1).count() == 0:
    group = db.auth_group.insert(
            id = 1,
            role = 'admin',
            description = 'administrator'
                                            )

    admin = db.auth_user.insert(
            id = 1,
            first_name = 'admin',
            last_name = 'admin',
            email = 'admin@pobook.ca',
            password = db.auth_user.password.validate('admin')[0]
            )

    membership = db.auth_membership.insert(
            id = 1,
            user_id = 1,
            group_id = 1
                                                    )

    db.commit()
